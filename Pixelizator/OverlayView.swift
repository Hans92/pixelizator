//
//  OverlayView.swift
//  Pixelizator
//
//  Created by Gabriel Zolnierczuk on 12/05/15.
//  Copyright (c) 2015 PJATK. All rights reserved.
//

import UIKit

protocol ButtonWasPressedDelegate: class {
    func shutterPressed()
}

class OverlayView: UIView {
    
    weak var buttonDelegate: ButtonWasPressedDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        let rect = CGRect(x: frame.width / 2, y: frame.height - 100, width: 100, height: 100)
        
        let shutter = UIButton(frame: rect)
        shutter.backgroundColor = UIColor.magentaColor()
        
        shutter.addTarget(self, action: Selector("shutterWasPressed:"), forControlEvents: UIControlEvents.TouchUpInside)
        self.addSubview(shutter)
    }

    func shutterWasPressed(sender: UIButton) {
        self.buttonDelegate?.shutterPressed()
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
