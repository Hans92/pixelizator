//
//  ViewController.swift
//  Pixelizator
//
//  Created by Kuba Reinhard on 05.05.2015.
//  Copyright (c) 2015 PJATK. All rights reserved.
//

import UIKit

class ViewController: UIImagePickerController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, ButtonWasPressedDelegate {

	override func viewDidLoad() {
		super.viewDidLoad()
        //delegate
        self.delegate = self
        //source type
        self.sourceType = UIImagePickerControllerSourceType.Camera
        //hide default controls
        self.showsCameraControls = false
        self.navigationBarHidden = true
        self.toolbarHidden = true
        //rozmiar ekranu
        var screenBounds: CGSize = UIScreen.mainScreen().bounds.size
        
        var scale = screenBounds.height / screenBounds.width
        
        let transform = CGAffineTransformScale(self.cameraViewTransform, scale, scale)
        
        self.cameraViewTransform = transform
        let overlayView = OverlayView(frame: self.view.frame)
        overlayView.buttonDelegate = self
        self.cameraOverlayView = overlayView
		// Do any additional setup after loading the view, typically from a nib.
	}
    func shutterPressed() {
        self.takePicture()
    }
    
    func imagePickerController(picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: [NSObject : AnyObject]!) {
        let 🌅 = editingInfo[UIImagePickerControllerOriginalImage] as? UIImage
        println(🌅)
        //image to nasze zdjecie, ktore bedziemy pozniej obrabiac
    }
    
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}


}

